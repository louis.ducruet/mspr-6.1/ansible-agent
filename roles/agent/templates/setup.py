#!/usr/bin/env python3
import os
import socket
import subprocess
import argparse

parser = argparse.ArgumentParser(
                    prog='harvester_setup',
                    description='Initializes and updates the harvester agent',
                    epilog='by Karl L., Nathan P. et Louis D. for MSpr 6.1')
parser.add_argument('-u', '--uri', type=str, required=True,
                    help='The URI used by the agent to contact the API (REQUIRED)')
parser.add_argument('-t', '--token', type=str, required=True,
                    help='The token used by the agent to contact the API (REQUIRED)')
parser.add_argument('-q', '--quiet', action='store_true',
                    help='Prints only errors and warnings. Use it if you want to run the script in automation.')
args = parser.parse_args()

with open(os.devnull, 'wb') as devnull:
    try:
        if subprocess.check_call(['docker', 'compose'], stdout=devnull, stderr=subprocess.STDOUT) == 0:
            compose_prefix = "docker compose"
    except subprocess.CalledProcessError:
        try:
            if subprocess.check_call(['docker-compose'], stdout=devnull, stderr=subprocess.STDOUT) == 0:
                compose_prefix = 'docker-compose'
        except subprocess.CalledProcessError:
            raise Exception("'docker compose' is not an executable. Please install it.")

compose_names = {
    "docker-compose.yml",
    "docker-compose.yaml",
    "compose.yaml",
    "compose.yml"
}

app_config = {
    "config.conf"
}

OKGREEN = '\033[92m'
WARNORANGE = '\033[93m'
ENDC = '\033[0m'
color = lambda x: f"{OKGREEN}{x}{ENDC}"
warning = lambda x: f"{WARNORANGE}{x}{ENDC}"


def stdout(message):
    if not args.quiet:
        print(message)


def get_file_path(dir, file_names, default_file_name, default_content):
    # check if dir exist
    if not os.path.isdir(dir):
        raise NotADirectoryError(f"'{dir}'")

    # check if dir contains the file
    file = file_names.intersection(os.listdir(dir))
    if len(file) == 0:
        # create file with default content
        stdout(f"Init : Directory '{dir}' doesn't contain a compose file in one of the following forms: {file_names}")
        with open(os.path.join(dir, default_file_name), 'w') as f:
            f.write(default_content)
        stdout(f"Init : Created {color(default_file_name)} in {color(dir)}")
        return os.path.join(dir, default_file_name)
    if len(file) > 1:
        raise Exception(f"Directory '{dir}' contains more then one file, namely: {file}")
    return os.path.join(dir, file.pop())


def setup_crontab():
    stdout(f"Init : Setting up crontab")
    cronjobs = [f"@daily python3 {os.getcwd()}/setup.py", f"@reboot python3 {os.getcwd()}/setup.py"]
    for cronjob in cronjobs:
        cron_exists = os.system(f'crontab -l | grep -q "{cronjob}"')
        if cron_exists != 0:
            os.system(f'crontab -l > /tmp/crontab')
            os.system(f'echo "{cronjob}" >> /tmp/crontab')
            os.system(f'crontab /tmp/crontab')
            stdout(f"Init : Added {color(cronjob)} to crontab")
        else:
            stdout(f"Init : {warning(cronjob)} already exists in crontab")


def pull_images(compose_path):
    stdout(f"Prepare : Pulling images for {color(compose_path)}")
    return os.system(f'{compose_prefix} -f {compose_path} pull --quiet')


def update_compose(compose_path):
    stdout(f"Update : Updating container(s) {color(compose_path)}")
    if args.quiet:
        return os.system(f'{compose_prefix} -f {compose_path} up -d')
    else:
        return os.system(f'{compose_prefix} -f {compose_path} up -d')


def purge_unused_images():
    stdout(f"Cleanup : Removing unused images")
    if args.quiet:
        return os.system(f'docker image prune -f >/dev/null')
    else:
        return os.system(f'docker image prune -f')


def update_composes():

    default_compose = f"""version: "3"
services:
  agent:
    image: registry.gitlab.com/louis.ducruet/mspr-6.1/agent:latest
    hostname: {socket.gethostname()}
    network_mode: host
    volumes:
      - ./config.conf:/app/config.conf
    restart: always
    """
    compose_file = get_file_path(os.getcwd(), compose_names, "docker-compose.yml", default_compose)
    default_config = f"""
[API]
url = {args.uri}
token = {args.token}
    
[Section1]
init = None
site_id = None
site = None
ville = None

[Section2]
agent_id = None
hostname = None
    """
    config_file = get_file_path(os.getcwd(), app_config, "config.conf", default_config)
    stdout(f"Init : Using {color(compose_file)} and {color(config_file)}")
    pull_images(compose_file)
    setup_crontab()
    update_compose(compose_file)
    stdout(f"Update : Agent is running as {color('http://' + socket.gethostbyname(socket.gethostname()) + ':8000')} or from another address of this machine")
    purge_unused_images()


if __name__ == "__main__":
    update_composes()
